﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;

namespace project_anemia
{
    class Program
    {
        //global variables used for generating output
        static int[] outputRowNumber = { 1, 1, 1, 1, 1, 1};

        static void Main(string[] args)
        {
            //variables used for initial values
            string initialValueFilePath = "C:\\Users\\Vihanga\\Documents\\Code\\project-anemia\\InitialData.csv";
            int initialTimeInterval = 20;
            double [] sensorInitialValues = new double[6];


            //variables used for testing
            string testingFilesPath = "C:\\Users\\Vihanga\\Documents\\Code\\project-anemia\\testingFiles";
            double measuringPercentage = 0.7;


            //variables used for generating output
            Excel.Application xlAppOutput = new Excel.Application();
            object misValue = System.Reflection.Missing.Value;
            Excel.Workbook xlWorkbookOutput = xlAppOutput.Workbooks.Add(misValue);
            Excel._Worksheet xlWorksheetOutput = xlWorkbookOutput.Sheets[1];
            string outputFilePath = "C:\\Users\\Vihanga\\Documents\\Code\\project-anemia\\testingFiles\\abcd.xlsx";

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine("=======PROJECT STARTED=======");

            //Get Innitial Values
            Console.WriteLine("\n Calculating initial values...");
            sensorInitialValues[0] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "K");
            sensorInitialValues[1] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "L");
            sensorInitialValues[2] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "M");
            sensorInitialValues[3] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "O");
            sensorInitialValues[4] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "P");
            sensorInitialValues[5] = getInitialAmplitude(initialValueFilePath, initialTimeInterval, "Q");

            //output write to console
            Console.WriteLine("\n Sensor initial Values" + sensorInitialValues[0]+" "+
                              sensorInitialValues[1] + " " +
                              sensorInitialValues[2] + " " +
                              sensorInitialValues[3] + " " +
                              sensorInitialValues[4] + " " +
                              sensorInitialValues[5]);

            stopWatch.Stop();
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            stopWatch.Elapsed.Hours, stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds,
            stopWatch.Elapsed.Milliseconds / 10);

            //get file names for testing
            Console.WriteLine("\n Files to be tested");
            string[] files = Directory.GetFiles(testingFilesPath);
            foreach (string file in files)
                Console.WriteLine(Path.GetFileName(file));
            Console.WriteLine("\n \n");

            Console.WriteLine("\n Time Executed: " + elapsedTime);
            Console.WriteLine("\n Executing test....");
            
            foreach (string file in files)
            {
                Console.WriteLine("\n \n File Name: " + Path.GetFileName(file));
                executeTest(testingFilesPath + "\\" + Path.GetFileName(file), 5, sensorInitialValues, measuringPercentage, xlWorksheetOutput);
            }

            //output file - check subsequent sensor total values
            Excel.Range xlRangeOutput = xlWorksheetOutput.UsedRange;
            int rowCountOutput = xlRangeOutput.Rows.Count;
            int sumNearby2 = 0;
            int sumNearby3 = 0;
            for (int i=1; i< rowCountOutput; i++)
            {
            
                if (xlRangeOutput.Cells[i, 7].Value2 >= 4)
                {
                    if (xlRangeOutput.Cells[i+1, 7].Value2 >= 4)
                    {
                        if (xlRangeOutput.Cells[i+2, 7].Value2 >= 4)
                        {
                            sumNearby3++;
                        }
                        else
                        {
                            sumNearby2++;
                        }
                    }
                }
            }
            xlWorksheetOutput.Cells[1, "H"] = "Total scenarios where 2 subsequent values are greater than or equal to 4: " + sumNearby2;
            xlWorksheetOutput.Cells[1, "I"] = "Total scenarios where 3 subsequent values are greater than or equal to 4: " + sumNearby3;

            //Save Output
            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlWorksheetOutput);

            //Prevent prompting alert boxes
            xlAppOutput.DisplayAlerts = false;
            //Forced Save
            xlWorkbookOutput.SaveAs(outputFilePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing);

            //close and release
            xlWorkbookOutput.Close();
            Marshal.ReleaseComObject(xlWorkbookOutput);

            //quit and release
            xlAppOutput.Quit();
            Marshal.ReleaseComObject(xlAppOutput);

        }

        public static double getInitialAmplitude(string filePath, int range, string column)
        {
            double amplitude=0;
            double maxValue;
            double minValue;
            int rowCount;
            int colCount;
            int maxOutputCell = 1;
            int minOutputCell = 1;
            int rangeFirstCell = 2;
            int rangeLastCell = range*1000 +1;
            int loopCount = 60/range;
            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            rowCount = xlRange.Rows.Count;
            colCount = xlRange.Columns.Count;
            

            for (int i=1; i<=loopCount; i++)
            {
                //Find and store Max Value in the column
                xlWorksheet.Range["AN" + maxOutputCell].Formula = "=MAX(" + column + rangeFirstCell + ":" + column + rangeLastCell + ")";
                maxValue = xlRange.Cells[maxOutputCell, 40].Value2;
                xlWorksheet.Range["AO" + minOutputCell].Formula = "=MIN(" + column + rangeFirstCell + ":" + column + rangeLastCell + ")";
                minValue = xlRange.Cells[minOutputCell, 41].Value2;

                rangeFirstCell = rangeLastCell + 1;
                rangeLastCell = rangeLastCell + (range*1000);
                xlWorksheet.Range["AP" + maxOutputCell].Formula= "=ABS(AN" + maxOutputCell + "-AO" + minOutputCell + ")";
                
                maxOutputCell++;
                minOutputCell++;
            }
            xlWorksheet.Range["AQ1"].Formula = "=AVERAGE(AP1:AP" + loopCount + ")";
            amplitude = 0;
            amplitude = xlRange.Cells[1, 43].Value2;

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //Prevent prompting alert boxes
            xlApp.DisplayAlerts = false;
            //Forced Save
            xlWorkbook.SaveAs(filePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
            
            return amplitude;
        }

        public static void executeTest(string filePath, int range, double [] initialAmplitude, double measuringPercentage, Excel._Worksheet xlWorksheetOutput)
        {
            double amplitude = 0;
            double maxValue;
            double minValue;
            int rowCount;
            int colCount;
            int maxOutputCell = 1;
            int minOutputCell = 1;
            int rangeFirstCell = 2;
            int rangeLastCell = range * 1000 + 1;
            int loopCount = 60 / range;
            string[] columnNames = {"K","L","M","O","P","Q" };
            string[] OutputColumnNames = { "A", "B", "C", "D", "E", "F" };
            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            rowCount = xlRange.Rows.Count;
            colCount = xlRange.Columns.Count;            
           

            Console.WriteLine("\n Calculating amplitude for each column - Range " + range + " seconds.");
            for (int columnIndex=0; columnIndex<6; columnIndex++)
            {
                rangeFirstCell = 2;
                rangeLastCell = (range * 1000);
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                Console.WriteLine("\n Calculating amplitude for " + columnNames[columnIndex] + " column");
                Console.WriteLine("Initial amplitude: "+initialAmplitude[columnIndex]);
                for (int i = 1; i <= loopCount; i++)
                {
                    double previousAmplitude = 0;
                    double laterAmplitude = 0;
                    if (i != 1)
                    {
                        xlWorksheet.Range["AQ1"].Formula = "=MAX(" + columnNames[columnIndex] + (rangeFirstCell - 3000) + ":" + columnNames[columnIndex] + (rangeFirstCell - 1) + ")";
                        maxValue = xlRange.Cells[1, 43].Value2;
                        xlWorksheet.Range["AQ2"].Formula = "=MIN(" + columnNames[columnIndex] + (rangeFirstCell - 3000) + ":" + columnNames[columnIndex] + (rangeFirstCell - 1) + ")";
                        minValue = xlRange.Cells[2, 43].Value2;
                        previousAmplitude = Math.Abs(maxValue-minValue);

                        xlWorksheet.Range["AQ1"].Formula = "=MAX(" + columnNames[columnIndex] + (rangeLastCell + 1) + ":" + columnNames[columnIndex] + (rangeLastCell + 3000) + ")";
                        maxValue = xlRange.Cells[1, 43].Value2;
                        xlWorksheet.Range["AQ2"].Formula = "=MIN(" + columnNames[columnIndex] + (rangeLastCell + 1) + ":" + columnNames[columnIndex] + (rangeLastCell + 3000) + ")";
                        minValue = xlRange.Cells[2, 43].Value2;
                        laterAmplitude = Math.Abs(maxValue - minValue);

                        maxValue = 0;
                        minValue = 0;
                    }

                    if ((i==1) || ((previousAmplitude/initialAmplitude[columnIndex])>(laterAmplitude/initialAmplitude[columnIndex])))
                    {
                        //Find and store Max and Min Values in the column
                        xlWorksheet.Range["AN" + maxOutputCell].Formula = "=MAX(" + columnNames[columnIndex] + rangeFirstCell + ":" + columnNames[columnIndex] + rangeLastCell + ")";
                        maxValue = xlRange.Cells[maxOutputCell, 40].Value2;
                        xlWorksheet.Range["AO" + minOutputCell].Formula = "=MIN(" + columnNames[columnIndex] + rangeFirstCell + ":" + columnNames[columnIndex] + rangeLastCell + ")";
                        minValue = xlRange.Cells[minOutputCell, 41].Value2;


                        Console.WriteLine("Amplitude for Cell " + rangeFirstCell + " to " + rangeLastCell + ": "+(((Math.Abs(maxValue-minValue))/ initialAmplitude[columnIndex])< measuringPercentage));

                        //write to output file
                        if ((((Math.Abs(maxValue - minValue)) / initialAmplitude[columnIndex]) < measuringPercentage))
                            xlWorksheetOutput.Cells[outputRowNumber[columnIndex], OutputColumnNames[columnIndex]] = 1;
                        else
                        {
                            xlWorksheetOutput.Cells[outputRowNumber[columnIndex], OutputColumnNames[columnIndex]] = 0;
                        }

                        rangeFirstCell = rangeLastCell + 1;
                        rangeLastCell = rangeLastCell + (range*1000);

                        //output - calculate sum
                        if (columnIndex == 5)
                        {
                            xlWorksheetOutput.Range["G" + outputRowNumber[columnIndex]].Formula = "=SUM(A" + outputRowNumber[columnIndex] + ":F" + outputRowNumber[columnIndex] + ")";
                        }

                        maxOutputCell++;
                        minOutputCell++;
                        outputRowNumber[columnIndex]++;
                    }
                    else
                    {
                        Console.WriteLine("Amplitude for Cell " + rangeFirstCell + " to " + rangeLastCell + ": Did not meet the requirements.");
                        rangeFirstCell = rangeLastCell + 1;
                        rangeLastCell = rangeLastCell + (range*1000);
                        xlWorksheetOutput.Cells[outputRowNumber[columnIndex], OutputColumnNames[columnIndex]] = "";


                        //output - calculate sum
                        if (columnIndex == 5)
                        {
                            xlWorksheetOutput.Range["G" + outputRowNumber[columnIndex]].Formula = "=SUM(A" + outputRowNumber[columnIndex] + ":F" + outputRowNumber[columnIndex] + ")";
                        }

                        maxOutputCell++;
                        minOutputCell++;
                        outputRowNumber[columnIndex]++;
                    }
                    
                }
                stopWatch.Stop();
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                stopWatch.Elapsed.Hours, stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds,
                stopWatch.Elapsed.Milliseconds / 10);
                Console.WriteLine("Time executed for " + columnNames[columnIndex] + " column: " + elapsedTime);

            }
            //xlWorksheet.Range["AQ1"].Formula = "=AVERAGE(AP1:AP" + loopCount + ")";
            amplitude = 0;
            amplitude = xlRange.Cells[1, 43].Value2;

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //Prevent prompting alert boxes
            xlApp.DisplayAlerts = false;
            //Forced Save
            xlWorkbook.SaveAs(filePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

        }


       

    }
}
